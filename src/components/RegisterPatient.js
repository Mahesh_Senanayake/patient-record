import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { registerpatient } from "../actions/auth";
import { Redirect } from "react-router";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vname = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The name must be between 3 and 20 characters.
      </div>
    );
  }
};

const vphonenumber = (value) => {
  if (value.length !== 10) {
    return (
      <div className="alert alert-danger" role="alert">
        The phone number must have 10 characters.
      </div>
    );
  }
};

const vnic = (value) => {
  if (value.length !== 10 && value.length !== 12) {
    return (
      <div className="alert alert-danger" role="alert">
        The nic number must have 10 or 12 characters.
      </div>
    );
  }
};

const Registerpatient = () => {
  const { user: currentUser } = useSelector((state) => state.auth);

  const form = useRef();
  const checkBtn = useRef();

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [nicnumber, setNicnumber] = useState("");
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const [dateofbirth, setDateofbirth] = useState("");
  const [gender, setGender] = useState("");
  const [height, setHeight] = useState("");
  const [weight, setWeight] = useState("");
  const [successful, setSuccessful] = useState(false);

  const { message } = useSelector((state) => state.message);
  const dispatch = useDispatch();

  const onChangeFirstname = (e) => {
    const firstname = e.target.value;
    setFirstname(firstname);
  };

  const onChangeLastname = (e) => {
    const lastname = e.target.value;
    setLastname(lastname);
  };

  const onChangeNicnumber = (e) => {
    const nicnumber = e.target.value;
    setNicnumber(nicnumber);
  };

  const onChangeAddress = (e) => {
    const address = e.target.value;
    setAddress(address);
  };

  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };

  const onChangePhonenumber = (e) => {
    const phonenumber = e.target.value;
    setPhonenumber(phonenumber);
  };

  const onChangeDateofbirth = (e) => {
    const dateofbirth = e.target.value;
    setDateofbirth(dateofbirth);
  };

  const onChangeGender = (e) => {
    const gender = e.target.value;
    setGender(gender);
  };

  const onChangeHeight = (e) => {
    const height = e.target.value;
    setHeight(height);
  };

  const onChangeWeight = (e) => {
    const weight = e.target.value;
    setWeight(weight);
  };

  const handleRegister = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(
        registerpatient(
          firstname,
          lastname,
          nicnumber,
          address,
          email,
          phonenumber,
          dateofbirth,
          gender,
          height,
          weight
        )
      )
        .then(() => {
          setSuccessful(true);
        })
        .catch(() => {
          setSuccessful(false);
        });
    }
  };

  if (!currentUser) {
    return <Redirect to="/login" />;
  }

  return (
    <div className="col-md-12">
      <div className="card card-container">
        <img
          src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
          alt="profile-img"
          className="profile-img-card"
        />

        <Form onSubmit={handleRegister} ref={form}>
          {!successful && (
            <div>
              <div className="form-group">
                <label htmlFor="firstname">First Name</label>
                <Input
                  type="text"
                  className="form-control"
                  name="firstname"
                  value={firstname}
                  onChange={onChangeFirstname}
                  validations={[required, vname]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="lastname">Last Name</label>
                <Input
                  type="text"
                  className="form-control"
                  name="lastname"
                  value={lastname}
                  onChange={onChangeLastname}
                  validations={[required, vname]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="nicnumber">NIC Number</label>
                <Input
                  type="text"
                  className="form-control"
                  name="nicnumber"
                  value={nicnumber}
                  onChange={onChangeNicnumber}
                  validations={[required, vnic]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="address">Address</label>
                <Input
                  type="text"
                  className="form-control"
                  name="address"
                  value={address}
                  onChange={onChangeAddress}
                  validations={[required]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="email">E mail</label>
                <Input
                  type="text"
                  className="form-control"
                  name="email"
                  value={email}
                  onChange={onChangeEmail}
                  validations={[required, validEmail]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="phonenumber">Phone Number</label>
                <Input
                  type="text"
                  className="form-control"
                  name="phonenumber"
                  value={phonenumber}
                  onChange={onChangePhonenumber}
                  validations={[required, vphonenumber]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="dateofbirth">Date of Birth</label>
                <Input
                  type="date"
                  className="form-control"
                  name="dateofbirth"
                  value={dateofbirth}
                  onChange={onChangeDateofbirth}
                  validations={[required]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="gender">Gender</label>
                <select
                  className="form-control"
                  name="gender"
                  value={gender}
                  onChange={onChangeGender}
                  validations={[required]}
                >
                  <option value="">Select Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="height">Height</label>
                <Input
                  type="text"
                  className="form-control"
                  name="height"
                  value={height}
                  onChange={onChangeHeight}
                  validations={[required]}
                />
              </div>

              <div className="form-group">
                <label htmlFor="nicnumber">Weight</label>
                <Input
                  type="text"
                  className="form-control"
                  name="weight"
                  value={weight}
                  onChange={onChangeWeight}
                  validations={[required]}
                />
              </div>

              <div className="form-group">
                <button className="btn btn-primary btn-block">
                  Register Patient
                </button>
              </div>
            </div>
          )}

          {message && (
            <div className="form-group">
              <div
                className={
                  successful ? "alert alert-success" : "alert alert-danger"
                }
                role="alert"
              >
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
    </div>
  );
};

export default Registerpatient;
